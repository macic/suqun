package com.itheima.Service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.api.Sku;

public interface SkuService extends IService<Sku> {
}
