package com.itheima.Service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.Service.SkuService;
import com.itheima.api.Sku;
import com.itheima.mapper.SkuMapper;
import org.springframework.stereotype.Service;


@Service
public class SkuMapperImpl extends ServiceImpl<SkuMapper,Sku> implements SkuService {

}
