package com.itheima.api;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("tb_sku")
public class Sku implements Serializable {
@TableId(value = "id")
    private Long id;
    private String sn;
    private String name;
    private Integer price;
    private Integer num;
    private Integer alertNum;
    private String image;
    private String images;
    private Integer weight;
    private Date createTime;
    private Date updateTime;
    private String spuId;
    private Integer categoryId;
    private String categoryName;
    private String brandName;
    private String spec;
    private Integer saleNum;
    private Integer commentNum;
    private Character  status;


}
